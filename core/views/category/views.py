from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render,redirect
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView

from core.forms import CategoryForm
from core.models import Category
from core.models import Category, Product
from django.utils.decorators import method_decorator

#vistas basadas en funciones,
def category_list(request):
    data = {
        'title': 'Listado de Categorias',
        'categories': Category.objects.all()
    }
    return render(request, 'category/list.html', data)

#vistas basadas en clases, sobreescribiendo los valores de la vista
class CategoryListView(ListView):
    model = Category
    template_name = 'category/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args ,**kwargs):#se encarga de direccionar los parametro de () a get
        return super().dispatch(request, *args ,**kwargs)

    def post(self,request, *args ,**kwargs):
        data = {}
        try:
            data = Category.objects.get(pk=request.POST['id']).toJSON()
            #data['name'] = cat.name
        except Exception as e:
            data['error'] = str(e)
        #print(request.POST)
        return JsonResponse(data)

    #def get_queryset(self):
    #    return Category.objects.all()#filter(name__startswith='C')#Product.objects.all()#

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Categorias'
        #print(reverse_lazy('web:category_list'))
        #context['object_list'] = Product.objects.all()#trae los datos del producto sinlistar la categoria
        return context

class CategoryCreateView(CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'category/create.html'
    success_url = reverse_lazy('web:category_list')

    #def post(self, request, *args, **kwargs):
    #    print(request.POST)
    #    form = CategoryForm(request.POST)
    #    if form.is_valid():#preguntando si el formulario es valido
    #        form.save()
    #        return HttpResponseRedirect(self.success_url)
    #    self.object = None#para que no pasa nada, ya q no se crea la categoria cuando se repite
    #    context = self.get_context_data(**kwargs)
    #    context['form']=form
    #    return render(request, self.template_name,context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Crear una Categoria'
        return context