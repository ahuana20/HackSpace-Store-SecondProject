"""
WSGI app for app project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
#SE IMPORTA LA CONFIG DJANGO
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')
#aqui se ejecuta Dajngo por un protocolo WSGI
application = get_wsgi_application()
